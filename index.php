<!DOCTYPE html>
<html>
<head>
	<title>JQuery Form Validation</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<br><br>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3>Form Validations</h3>
				<form action="" method="POST">
					<div class="form-group">
						<label for="username">Username:</label>
					    <input type="text" name="username" class="form-control" placeholder="username" id="username">
					    <span class="err-msg"></span>
					</div>
					<div class="form-group">
						<label for="email">Email address:</label>
					    <input type="email" name="email" class="form-control" placeholder="email" id="email">
					    <span class="err-msg"></span>
					</div>
					<div class="form-group">
					    <label for="pwd">Password:</label>
					    <input type="password" name="password" class="form-control" placeholder="password" id="pwd">
					    <span class="err-msg"></span>
					</div>
					<div class="form-group">
						<label for="phone">Phone number:</label>
					    <input type="text" name="phone" id="phone-number" class="form-control" placeholder="phone number" id="phone">
					    <span class="err-msg"></span>
					</div>
					<input type="submit" name="submit" value="Submit" class="btn btn-primary">
				</form>
			</div>
			<div class="col-md-6">
				<h3>Results</h3>
				<pre><?php print_r($_POST) ?></pre>
			</div>
		</div>
	</div>

	<script src="assets/js/jquery-3.4.1.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
</body>
</html>