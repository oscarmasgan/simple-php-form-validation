$(function(){
    'use strict';
    $('form').on('submit', function(e){
        var err = 0, msg = '';
        $('form input').each(function(){
            var $this = $(this);
            if ( !$this.val() ) {
                msg = '*Bidang '+$this.attr('placeholder')+' harus diisi.';
                $this.siblings('.err-msg').text(msg).css('display', 'block');
                err = err+1;
            }else{
                $this.siblings('.err-msg').text(msg).css('display', 'none');
            }
            if ( $this.attr('type') == 'email' && $this.val() !== '' ){
                if (emailIsValid( $this.val() ) == false ) {
                    $this.siblings('.err-msg').text('Format email tidak valid !').css('display', 'block');
                    err = err+1;
                }
                $this.attr('type');
            }

            if ( $this.attr('id') == 'phone-number' && $this.val() !== '' ){
                if ( !validatePhone("phone-number") ){
                    $this.siblings('.err-msg').text('Format nomor telepon salah !').css('display', 'block');
                    err = err+1;
                }else{
                    $this.siblings('.err-msg').text('').css('display', 'none');
                }
            }
        });

        if ( err !== 0 ) { e.preventDefault(); }
    });

});

function emailIsValid(email){
    return /\S+@\S+\.\S+/.test(email);
}

function validatePhone(e) {
    var t = document.getElementById(e).value;
    return !!/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/.test(t);
}